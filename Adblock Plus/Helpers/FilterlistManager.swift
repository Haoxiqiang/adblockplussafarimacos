/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

class FilterlistManager {

    /// Shared UserDefaults
    private let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)

    /// - Parameter language: The language variant of the filterlist to be returned
    /// - Returns: A filterlist for the selected language, or the default filterlist if a language variant cannot be found.
    func getFilterlistWithLanguage(_ language: FilterlistLanguage) -> Filterlist {
        let filterlists = Filterlists.filterlists
        return filterlists.first(where: { $0.language == language }) ?? filterlists[0]
    }

    /// - Returns: Sets default filterlist based on the users prefered language
    private func setDefaultFilterlist() -> Filterlist {
        var filterlist: Filterlist
        let defaultCode = Locale.preferredLanguages[0]
        switch defaultCode.prefix(2) {
        case "de":
            filterlist = getFilterlistWithLanguage(.german)
        case "fr":
            filterlist = getFilterlistWithLanguage(.french)
        case "nl":
            filterlist = getFilterlistWithLanguage(.dutch)
        case "es":
            filterlist = getFilterlistWithLanguage(.spanish)
        case "zh":
            filterlist = getFilterlistWithLanguage(.chinese)
        case "it":
            filterlist = getFilterlistWithLanguage(.italian)
        case "ru":
            filterlist = getFilterlistWithLanguage(.russian)
        case "en":
            filterlist = getFilterlistWithLanguage(.english)
        case "vn":
            filterlist = getFilterlistWithLanguage(.vietnamese)
        default:
            filterlist = getFilterlistWithLanguage(.english)
        }

        groupDefaults?.set(filterlist.language.rawValue, forKey: Constants.selectedFilterList)

        return filterlist
    }

    /// Copies filter lists from Main Bundle to Shared Container
    /// To be used when app is first run, or to update the filter lists during app update.
    func copyFilterlistsToSharedContainer() {
        // This saves 2 copies of the blocklist. An original, and one that can be mutated as per the allowlist.

        var filterlist: Filterlist?

        if let selectedFilterlist = groupDefaults?.string(forKey: Constants.selectedFilterList) {
            filterlist = getFilterlistWithLanguage(FilterlistLanguage(rawValue: selectedFilterlist) ?? .english)
        } else {
            filterlist = setDefaultFilterlist()
        }

        guard let uwFilterList = filterlist else { return }

        // Create a path to filter lists stored in local bundle
        let bundledFilterlistFilePath = Bundle.main.url(forResource: uwFilterList.files[0], withExtension: "json")
        let bundledFilterlistExceptionsFilePath = Bundle.main.url(forResource: uwFilterList.files[1], withExtension: "json")

        // Store local bundled filterlists as data object
        let filterlistData = try? Data(contentsOf: bundledFilterlistFilePath!)
        let filterlistExceptionsData = try? Data(contentsOf: bundledFilterlistExceptionsFilePath!)

        // Create a path to the shared container for filterlists to be copied to
        let sharedContainer = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: Constants.groupIdentifier)
        let sharedFilterlistPath = URL(string: "\(Constants.sharedFilterlist).json", relativeTo: sharedContainer)
        let sharedFilterlistOriginalPath = URL(string: "\(Constants.sharedFilterlistOriginal).json", relativeTo: sharedContainer)
        let sharedFilterlistExceptionsPath = URL(string: "\(Constants.sharedFilterlistWithExceptions).json", relativeTo: sharedContainer)
        let sharedFilterlistExceptionsOriginalPath = URL(string: "\(Constants.sharedFilterlistWithExceptionsOriginal).json",
                                                         relativeTo: sharedContainer)

        // Copy local bundled filterlist data to shared container
        do {
            try filterlistData?.write(to: sharedFilterlistPath!, options: [.atomic])
            try filterlistData?.write(to: sharedFilterlistOriginalPath!, options: [.atomic])
            try filterlistExceptionsData?.write(to: sharedFilterlistExceptionsPath!, options: [.atomic])
            try filterlistExceptionsData?.write(to: sharedFilterlistExceptionsOriginalPath!, options: [.atomic])
        } catch {
        }
    }
}
