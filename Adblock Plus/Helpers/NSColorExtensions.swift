/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa

/// An enum of the custom colors added in the asset catalog
enum CustomColor {
    case graySubtext
    case onboardingBorder
    case sidebarItemSelected
}

extension NSColor {

    /// Links custom colors defined in the asset catalog to NSColor objects.
    /// Falls back to default colors on macOS versions where named colours are not supported.
    /// - Returns: An NSColor object defined inside the
    static func customColor(_ name: CustomColor) -> NSColor {
        switch name {
        case .graySubtext:
            if #available(OSX 10.13, *) {
                return NSColor(named: "Gray Subtext Color") ?? NSColor.gray
            } else {
                // Fallback on earlier versions
                return NSColor.gray
            }
        case .onboardingBorder:
            if #available(OSX 10.13, *) {
                return NSColor(named: "Onboarding Border Color") ?? NSColor(calibratedWhite: 0.0, alpha: 0.15)
            } else {
                // Fallback on earlier versions
                return NSColor(calibratedWhite: 0.0, alpha: 0.15)
            }
        case .sidebarItemSelected:
            if #available(OSX 10.13, *) {
                return NSColor(named: "SidebarItem Selected Color") ?? NSColor.init(white: 0.5, alpha: 0.1)
            } else {
                // Fallback on earlier versions
                return NSColor.init(white: 0.5, alpha: 0.1)
            }
        }
    }
}
