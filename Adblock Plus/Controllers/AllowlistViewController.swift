/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class AllowlistViewController: NSViewController {

    private let notification = Notification.Name(rawValue: "\(Constants.safariToolbarIdentifier).allowlist")
    private let allowlistItemDataSource = AllowlistItemDataSource()

    @IBOutlet weak var allowlistTitle: NSTextFieldCell!
    @IBOutlet weak var urlTextField: NSTextField!
    @IBOutlet weak var addWebsiteButton: NSButton!
    @IBOutlet weak var allowlistCollectionView: NSCollectionView!

    @IBAction func removeFromAllowlistButton(_ sender: NSButton) {
        // Get URL from sender's cell.
        let indexPath = IndexPath(item: sender.tag, section: 0)
        let cell = allowlistCollectionView.item(at: indexPath)
        guard let url = cell?.textField?.stringValue else { return }

        // Remove URL from array
        let allowlistDataSource = AllowlistItemDataSource()
        allowlistDataSource.removeURLfromArray(url: url)
        let container = Constants.groupIdentifier
        let groupDefaults = UserDefaults(suiteName: container)

        // Get new array, and apply allowlist to blocklists and reload contentblocker and collection view.
        guard let allowlistArray = groupDefaults?.stringArray(forKey: Constants.allowlistArray) else { return }
        let allowlistManager = AllowlistManager()
        allowlistManager.applyAllowlist(allowlistArray)
        SFContentBlockerManager.reloadContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { _ in
        }
        allowlistCollectionView.reloadData()
    }

    @IBAction func urlTextFieldEntered(_ sender: NSTextField) {

        // If this print statement is removed, for some reason, `addWebsiteToAllowlist` func does not work ¯\_(ツ)_/¯
        print(sender.stringValue)

        let allowlistArray = allowlistItemDataSource.getAllowlistArray()

        // Disable UI Elements
        sender.isEnabled = false
        addWebsiteButton.isEnabled = false

        // Format hostname string
        guard !sender.stringValue.isEmpty,
            let hostname = NSString(string: sender.stringValue).allowlistHostname(),
            !allowlistArray.contains(hostname) else {
                sender.isEnabled = true
                sender.stringValue = ""
                addWebsiteButton.isEnabled = true
                return
        }

        // Add Hostname to AllowlistArray
        let allowlistItemDataSource = AllowlistItemDataSource()
        allowlistItemDataSource.appendToAllowlist(hostname: hostname)

        let modifiedAllowlistArray = allowlistItemDataSource.getAllowlistArray()

        // Reload NSCollectionView
        allowlistCollectionView.reloadData()

        // Apply Allowlist
        let allowlistManager = AllowlistManager()
        allowlistManager.applyAllowlist(modifiedAllowlistArray)

        // Reload Content Blocker
        SFContentBlockerManager.reloadContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { _ in
        }
        // Enable UI Elements
        sender.stringValue = ""
        sender.isEnabled = true
        addWebsiteButton.isEnabled = true
    }

    @IBAction func addWebsiteToAllowlist(_ sender: NSButton) {
        urlTextFieldEntered(urlTextField)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        DistributedNotificationCenter.default().addObserver(self,
                                                            selector: #selector(self.updateAllowlist),
                                                            name: notification,
                                                            object: Constants.safariToolbarIdentifier)
    }

    override func viewWillAppear() {
        super.viewWillAppear()
        allowlistTitle.title = "Allowlist".localized
    }

    @objc
    func updateAllowlist() {
        allowlistCollectionView.reloadData()
    }
}

// Extends ViewController to act as data source for the Allow NSCollectionView
extension AllowlistViewController: NSCollectionViewDataSource {

    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberOfItems = allowlistItemDataSource.numberOfAllowlistItems()
        return numberOfItems
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let allowlistItem = NSUserInterfaceItemIdentifier(rawValue: Constants.allowlistItem)

        // swiftlint:disable:next force_cast
        let item = collectionView.makeItem(withIdentifier: allowlistItem, for: indexPath) as! AllowlistItem
        item.textField?.stringValue = allowlistItemDataSource.allowlistItemForIndexPath(indexPath: indexPath)
        item.trashcanOutlet.tag = indexPath.item
        item.trashcanOutlet.isHidden = true

        return item
    }

}
